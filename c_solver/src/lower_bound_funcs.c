#include <assert.h>

#include "graph.h"
#include "colors.h"
#include "lower_bound_funcs.h"


int get_lower_bound_0(Colors_T *C){
    return C->nb_colors_used;
}


LowerBoundFunc_T *get_lower_bound_func(int index){
    switch(index){
    case 0:
        return get_lower_bound_0;
    default:
        assert(0 && "Error ! unknown lower_bound_func index\n");
        return NULL;
    }
}
