#include <stdlib.h>
#include <stdio.h>

#include "graph.h"
#include "colors.h"
#include "lower_bound_funcs.h"
#include "upper_bound_funcs.h"
#include "branching_vertex_choice_funcs.h"
#include "cbfs_heap.h"
#include "heap_cmp_funcs.h"
#include "branch_and_bound.h"

Graph_T *G;

int main(int argc, char *argv[]){
    if (argc < 6){
        printf("usage : %s filename heap_cmp_func lower_bound_func upper_bound_func branching_vertex_choice_func\nExiting.\n", argv[0]);
        return -1;
    }

    FILE *f = fopen(argv[1], "rb");
    if (!f){
        printf("Error. Can not open file %s.\nExiting.\n", argv[1]);
    }
    G = init_graph_from_file(f);

    HeapCmpFunc_T **heap_cmp_funcs = get_heap_cmp_funcs(atoi(argv[2]));
    LowerBoundFunc_T *get_lower_bound = get_lower_bound_func(atoi(argv[3]));
    UpperBoundFunc_T *get_upper_bound = get_upper_bound_func(atoi(argv[4]));
    BranchingVertexChoiceFunc_T *choose_branching_vertex =
        get_branching_vertex_choice_func(atoi(argv[5]));
    int nb_colors = vertex_coloring_branch_and_bound(
                                                     heap_cmp_funcs,
                                                     get_lower_bound,
                                                     get_upper_bound,
                                                     choose_branching_vertex
                                                     );
    printf("found %d colors\n", nb_colors);
}
