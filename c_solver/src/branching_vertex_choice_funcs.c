#include <stdlib.h>
#include <assert.h>

#include "graph.h"
#include "colors.h"
#include "branching_vertex_choice_funcs.h"


BranchingVertexChoiceFunc_T *get_branching_vertex_choice_func(int index){
    switch(index){
    case 0:
        return branching_vertex_choice_func_0;
    default:
        assert(0 && "Error ! unknown branching_vertex_choice_func index\n");
    }
}

int branching_vertex_choice_func_0(Colors_T *C){
    int u;
    for (u = 0; u < G->nb_vertices; u++){
        if (C->colors[u] == UNCOLORED){
            break;
        }
    }
    return u;
}
