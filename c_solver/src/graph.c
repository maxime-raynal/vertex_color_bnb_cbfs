#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <stdio.h>


#include "graph.h"

Graph_T *graph_init(int nb_vertices){
    Graph_T *GG = malloc(sizeof(Graph_T));
    GG->nb_vertices = nb_vertices;
    int **adj_matrix = malloc(nb_vertices * sizeof(int *));
    for (int i = 0; i < nb_vertices; i++){
        adj_matrix[i] = malloc(nb_vertices * sizeof(int));
        for (int j = 0; j < nb_vertices; j++){
            adj_matrix[i][j] = NO_EDGE;
        }
    }
    GG->adj_matrix = adj_matrix;
    return GG;
}

void graph_add_edge(Graph_T *GG, int u, int v){
    GG->adj_matrix[u][v] = GG->adj_matrix[v][u] = EXISTS_EDGE;
}

int graph_get_degree(Graph_T *GG, int u){
    int result = 0;
    for (int v = 0; v < GG->nb_vertices; v++){
        if (GG->adj_matrix[u][v] == EXISTS_EDGE){
            result ++;
        }
    }
    return result;
}


Graph_T *init_graph_from_string(char *graph_as_str){
    Graph_T *GG = NULL;
    const char delim[2] = "\n";
    char *tok, *ttok = strtok(graph_as_str, delim);
    int u, v;

    while (ttok != NULL){
        tok = ttok;
        switch(tok[0]){
        case('c'):
            break;

        case('p'):
            while (!isdigit(*(++tok)))
                { ; }
            GG = graph_init(atoi(tok));
            break;

        case('e'):
            assert(GG != NULL && "Error!\nIn init_graph_from_string, problem line missing\n");
            while (!isdigit(*(++tok)))
                { ; }
            u = atoi(tok);
            while (*(++tok) != ' ')
                { ; }
            v = atoi(tok);
            graph_add_edge(GG, u, v);
            break;

        default:
            assert(0 && "Error !\nIn init_graph_from_str : invalid line identifier");
        }
        ttok = strtok(NULL, delim);
    }
    return GG;
}


Graph_T *init_graph_from_file(FILE *file){
    char *file_as_string = NULL;
    long file_length;
    if (file){
        fseek(file, 0, SEEK_END);
        file_length = ftell(file);
        fseek(file, 0, SEEK_SET);

        file_as_string = malloc((file_length + 1) * sizeof(char));
        size_t fread_res = fread(file_as_string, 1, file_length, file);
        assert(fread_res == (size_t)file_length);
    }
    return init_graph_from_string(file_as_string);
}


char *graph_to_str(Graph_T *GG){
    char *str = malloc(GG->nb_vertices * GG->nb_vertices * 4 * sizeof(char));
    sprintf(str, "%d\n", GG->nb_vertices);
    for (int i = 0; i < GG->nb_vertices; i++){
        for (int j = 0; j < GG->nb_vertices; j++){
            strcat(str, (GG->adj_matrix[i][j] == NO_EDGE ? "0 " : "1 "));
                }
    }
    return str;
}
