#ifndef BRANCH_AND_BOUND_DOT_H
#define BRANCH_AND_BOUND_DOT_H

#include "graph.h"
#include "colors.h"
#include "cbfs_heap.h"
#include "lower_bound_funcs.h"
#include "upper_bound_funcs.h"
#include "heap_cmp_funcs.h"

int vertex_coloring_branch_and_bound(
              HeapCmpFunc_T **heap_cmp_funcs,
              LowerBoundFunc_T *get_lower_bound,
              UpperBoundFunc_T *get_upper_bound,
              BranchingVertexChoiceFunc_T *choose_branching_vertex);

#endif
