#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import itertools as it
import heapq as hq
from lib.graph import Graph, gen_random_connected_graph


def lower_bound(g, colors):
    """
    Returns a lower bound on the chromatic number of g for the partial
    solution colors
    """
    nb_colors = len(set(colors.values()))
    adj_to_all_colors = []
    vertices_to_color = set(g.vertices()) - set(colors)
    for u in vertices_to_color:
        if g.dsat(u, colors) == nb_colors:
            adj_to_all_colors += [u]
    for u, v in it.combinations(adj_to_all_colors, 2):
        if v in g.adj(u):
            return nb_colors+2
    return nb_colors+1 if adj_to_all_colors != set() else nb_colors


def upper_bound(g, c):
    """
    Returns an approximated solution of the vertex coloring of g.
    """
    colors = dict(c)
    vertices_to_color = set(g.vertices()) - set(colors)
    nb_colors = max(colors.values())
    while vertices_to_color != set():
        v_to_color = max(
            vertices_to_color, key=lambda u: (-g.dsat(u, colors), -g.degree(u))
        )
        adj_colors = {colors[v] for v in g.adj(v_to_color) if v in colors}
        available_colors = set(colors.values()) - adj_colors
        if not available_colors:
            nb_colors += 1
            colors[v_to_color] = nb_colors
        else:
            colors[v_to_color] = min(available_colors)
        vertices_to_color.remove(v_to_color)
    return nb_colors, colors


def get_chromatic_number(g):
    """
    Determines the chromatic number of an undirected graph using BnB
    Returns:
        C, solution where:
            C, int: the chromatic number of g
            sol, dict: an optimal solution
    """
    colors = {0: 1}
    next_v_to_color = 1
    C, sol = upper_bound(g, colors)
    q = [(lower_bound(g, colors), colors)]
    hq.heapify(q)
    while q:
        m, colors = hq.heappop(q)
        M, new_sol = upper_bound(g, colors)
        if M < C:
            C, sol = M, new_sol
        elif next_v_to_color < g.m_id:
            u = len(colors)
            adj_colors = {colors[v] for v in g.adj(u) if v in colors}
            available_colors = set(colors.values()) - adj_colors
            for color in available_colors | {max(colors.values())+1}:
                new_colors = dict(colors)
                new_colors[u] = color
                m = lower_bound(g, new_colors)
                if m < C:
                    hq.heappush(q, (m, new_colors))
    return C, sol


g = gen_random_connected_graph(8,11)
C, sol = get_chromatic_number(g)
print("chromatic number is %s with the coloring:\n%s" % (C, sol))
